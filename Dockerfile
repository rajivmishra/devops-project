FROM python:3.10.2-buster as base

ARG TODO_APP_ENV

ENV TODO_APP_ENV=${TODO_APP_ENV} \
# poetry env variables
POETRY_VERSION=1.1.13 \
  POETRY_NO_INTERACTION=1 \
  POETRY_VIRTUALENVS_CREATE=false \
  POETRY_CACHE_DIR='/var/cache/pypoetry' \
  POETRY_HOME='/usr/local'

#SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

RUN apt-get update && apt-get upgrade -y \
  && apt-get install --no-install-recommends -y \
    bash \
    build-essential \
    curl \
    gettext \
    git

RUN pip install "poetry==1.1.13" \
  && poetry --version

#Install Azure CLI
RUN pip install --upgrade pip
RUN pip install azure-cli

#EXPOSE 5000
RUN mkdir -p /opt/app
WORKDIR /opt/app
RUN groupadd -r web && useradd -d /opt/app -r -g web web \
 && chown web:web -R /opt/app
COPY --chown=web:web poetry.lock pyproject.toml /opt/app/

RUN echo "$TODO_APP_ENV" \
&& poetry version \
&& poetry run pip install -U pip \
&& poetry install \
$(if [ "$TODO_APP_ENV" = 'production' ]; then echo '--no-dev'; fi) --no-interaction --no-ansi \
&& if [ "$TODO_APP_ENV" = 'production' ]; then rm -rf "$POETRY_CACHE_DIR"; fi

#WORKDIR /opt/app
#COPY . .

FROM base as production
# Configure for production
RUN pip install  "gunicorn"
WORKDIR /opt/app/
COPY . .
WORKDIR /opt/app/Module1Exercise
EXPOSE 8080
CMD /usr/local/bin/gunicorn -b 0.0.0.0:${PORT:-8080} -t 30 --pythonpath /opt/app/ "todoapp:create_app()"

FROM base as development
# Configure for local development
WORKDIR /opt/app
COPY . .
RUN pip install  "flask"
EXPOSE 5000
ENTRYPOINT FLASK_APP=/opt/app/Module1Exercise/todoapp.py flask run --host=0.0.0.0

FROM base as test
# Configure for local testing
WORKDIR /opt/app
COPY . .
RUN pip install  "flask"
EXPOSE 5000
ENTRYPOINT ["poetry", "run", "pytest"]

