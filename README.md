# DevOps Apprenticeship: Project Exercise

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:
Execute the below commands on Python BASH/Powershelll
```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.
# Trello API Integration

As we would be using Trello For our API Integration exercise, we would need to follow few pre-requisites steps before we are able to start the integration. 
```
Step 1: Create a Trello Account (Sample account can be used too, but its better to have your own)
Step 2: Generate API Key. THis is the key which is displayed when a new account is created and want to integrate with Trello Boards. You can get your API key by logging into Trello and visiting https://trello.com/app-key. Save this value in the environmet file. Your API key would be a 32 character string comprised of random alphanumeric characters.
Step 3: Now for our exercise, we would need a API Token (we would call it as Server Token). It can be extracted from the same page as API-KEY. click the hyperlinked "Token" under the API key. The permissions, duration of access, and application name displayed are all configured via the URL parameters. We'll leave everything as is, and click "Allow".

NOTE: This token, along with your API key, can be used to read and write for your entire Trello account. Tokens should be kept secret!

```

Environment Variable Setup required to run the Application
1. Define the below 3 variables:
    API_KEY: THis is the key which is displayed when a new account is created and want to integrate with Trello Boards.
    SERVER_TOKEN: This is the token required for authentication.
    BOARD_ID: The board id which is where all our lists / cards are stored.

Also note the List ID for TODO, Doing and Done to ensure we can use them in the code for updating the card status. Though, these can be extracted via API calls, but would be handy if stored in the env variable for simplicity and avoiding compled code.

Further guides on: https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/

How to get Trello Board ID: Read the community guide - https://community.atlassian.com/t5/Trello-questions/How-to-get-Trello-Board-ID/qaq-p/1347525

There are few other key IDs in the board, namely ListID [idList] and Card ID [idCard]. The number of instances of these value would be dependent on the number of Lists and number of cards in each list.


# Module 3: Automated Testing

Pre-reqs: The new environment file needs to be created with .env.test name and placed within "test" folder (the folder under which all testing files are placed).
The contents has to be dummy values which are related to TRELLO. The Applicagtion Name and env has to be thr correct values.



Execute the below command to run the Automated testing:
```
poetry run pytest
```


# Module 4: Ansible Setup and automated Application deployment

We need minimum 2 nodes for ansible to be setup - one is called as Managed Node and other as Controller Node
Ansible client and server binaries should be installed.

```
On the Controller Node: 
create 2 folder:
 ansible_inventry - This contains the inventry file
 ansible_playbook - this contains the playbook (YAML) file
In the inventry folder, create a file called "inventry" and add the servers which need to be managed under different sections. Sample file:
    [webservers]
    18.135.197.117

```
Create a env template file called .env.j2 which would have all the environment variables needed for the applicatin being deployed.
The values for the environment should be added to Ansible Vault
    ansible-vault create /opt/todoapp/.env
To decrypt or encrypt file (the file is by default encrypted):
    ansible-vault decrypt .env --vault-password-file .password
    ansible-vault encrypt .env

```
To execute the YAML file, run the below command on the controller node:
```
    ansible-playbook first_playbook.yml -i ../ansible_inventory/inventory --vault-password-file .password

The password for the vault is stored on a file on the controller node to ensure one can run the command in an automated fashion if required without waiting for the password to be provided on the terminal.

# Module 5: Containeraize the application (Immutable Infrastructure)

```
We can build our containers using either of the below 2 methods:
a. Dockerfile with all commands and then running the docker build, docker push and docker run
b. docker-compose.yaml file which has all the options used to build/run the docker thus making our build process command simpler. 

```
Docker Build: We build the image of all dependencies along with OS into one file.
```
    e.g. docker build --target development --tag onlyrajivmishra/todo_app:dev .
```
Here, we are defining the environment, like development|testing|production, giving a tag name to the build file for easy identification along with tagging the build type similar to target. Note: the tag should have the username of the docker ub, if we need to push it on docker hub.


Docker Push: Pushes the build file to Docker hub.
```    
    docker push onlyrajivmishra/todo_app:dev
```
Docker Run: this command will execute the docker build.
```    
    e.g. docker run --env-file ./.env -p 5000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/opt/app/todo_app onlyrajivmishra/todo_app:dev
```
In the above example, we are providng a env file which is needed for the application/docker to run along with mount points to ensure the data is not lost when the build/image is deleted. We also provide a port mapping from local machine Port to container Port to ensure the docker and local machine can proxy the requests.

Docker Compose: This can reduce the number of commands one has to execute with multiple options into a single simple command.
```    
    docker-compose -f docker-compose.yaml up --build
```
We provide an input file which is usually called docker-compose. Depending on the configuration of gthe environment parameter, Docker would run the corresponding Web container - flask for development and gunicore for production. 
Note: To make the clear distinction of which web container is running, one can use individual port mapping, however its not mandatory.

Few other important Docker commands are:
    docker ps --> lists the docker processes running
    docker image list  --> List of all docker images in the local cache
    docker exec -it <docker image name / ID> sh --> this command can execute commands within the docker and is useful when we need to debug the tasks executing in a docker. The last parameter is just to list then once connected, open a shell.
    Docker commands with --d or -detach --> this would execute the command in the background. 


# Module 6: Architecture Diagrams

We have created based on C4 model - 3 diagrams
    - Context Diagram
    - Container Diagram
    - Component Diagram
All the above diagrams are saved under the documentation folder.
One can view the diagrams to understand about the Application being developed.

The diagrams were created using: https://app.diagrams.net/ tool, however any tool can be used based on ones comfort level.

To know more about the C4 model - https://c4model.com/


# Module 7: Continuous Integration
 
As part of CI, we would need to ensure we can build and test our application. We would use Docker which we learn in Module 5 and continue from there.

In Module 5, we have finally used Docker-compose which eliminate the need to sperate env file for docker and also automates the complete build and test actions. This also thus eliminates the need for instllation of dependencies on our machines and the package is built/tested within a docker container.

For CI to work in Gitlab:
1.  Set up GitLab CI for your repository 
Create the config file for your continuous integration pipeline:

- Create a file called .gitlab-ci.yml in the root of the project
Implement a basic workflow:

```
stages:
  - test    # This pipeline consists of a single stage called "test"

my-job:         # Start defining a job called "my-job"
  stage: test   # This job belongs to the "test" stage
  script:
    - echo "Hello, world"  # Run a script
```
Commit and push your changes to a branch on your repository.

On your repository page, navigate to the CI/CD tab.

You should see a table of pipelines. This should have one entry with a name matching your latest commit message. Select this entry.

You should see a page with a visual representation of your pipeline, you can click on a job to see the detailed console output.

If you haven't already, you will need to enter credit card details at this stage to enable use of the shared job runners. GitLab.com will show a notification if this is the case.

See the GitLab documentation for more details on how to get set up GitLab CI/CD and https://docs.gitlab.com/ee/ci/yaml/index.html for more details on the syntax of the gitlab-ci.yml file.

2. Implement CI pipeline
Now to build and test your code, as you are using docker compose, you can update the same command which you used for docker compose [docker-compose -f docker-compose.yaml up --build] by replacing the ECHO command in the sample file.


# Module 8: Continuous Deployment

The CI Pipeline in the previous exercise has been expanded to deploy the code into production after it has been tested successfully.

First we are deploying/pushing to Docker Hub and then to Heroku hosting platform.

Add Below variables to Gitlab CI/CD Varaibles which can then be called in the gitlab-ci.yaml file:
DOCKER_USERNAME
DOCKER_PASSWORD
HEROKU_API_KEY

Along with this, we need to add all the values from the .env file onto the Heroku Config Variables. The vaiables are like Trello - API_KEY, Board_ID, List_ID, Environment etc.

Before we start performing Automated deployment, we should test all the commands locally. Once the commands successed, add them to gitlab-ci.yaml and then finally push to prod.

```
# Log into the Heroku API if you haven't already 
$ heroku login 
# Log into the Heroku Container Registry (can also be achieved with docker login) 
$ heroku container:login 
# Tag the image you built earlier for Docker Hub with the correct name for Heroku 
$ docker tag <user_name>/<image_name> registry.heroku.com/<heroku_app_name>/web 
# Push it to Heroku registry 
$ docker push registry.heroku.com/<heroku_app_name>/web

We need to release the image on heroku to be able to access the application
heroku container:release web

Now one can run "Heroku Open" to automatically open the web app in your browser, or just enter "your-app.herokuapp.com" in your address bar, replacing "your-app" with your actual application name
```

App URL: https://rajiv-todo.herokuapp.com/

# Module 9: Continuous Deployment on Azure using Azure WebApps

we are using the code which was deployed as part of Module 8 with changes related to Heroku and replacing it with Azure.
Not much changes are required:
Once logged into Azure, execute the below commands to create the WebApp plan and the Webapp service.
```az appservice plan create --resource-group UBS21_RajivMishra_ProjectExercise -n UBS21_RajivMishraPEWeb --sku B1 --is-linux
az webapp create --resource-group UBS21_RajivMishra_ProjectExercise --plan UBS21_RajivMishraPEWeb --name rkmishratodo --deployment-container-image-name onlyrajivmishra/rajiv_todo_app:latest
```
We have a resource group already created with the name UBS21_RajivMishra_ProjectExercise
We already have on Docker a built image which we would use to deploy on Azure.

Step: Set up environment variables 
```
• Portal:
• Settings -> Configuration in the Portal
• Add all the environment variables as "New application setting
```

Once done, then check, if the appl is up and running. It can be accessed via the link:
https://<webapp_name>.azurewebsites.net/
In my case, http://rkmishratodo.azurewebsites.net/

Now we need to extract the webhookm URL from Azure Portal for our app for continuous deployment:
From your app service in the Azure portal, navigate to the Deployment Center in the left-hand menu
```
Test your webhook
• Take the webhook provided by the previous step, and run:
curl -dH -X POST your_webhook_url
• To prevent the `$username` part of the webhook being interpreted by your shell as a variable name, either wrap the URL in single quotes or put a backslash before the dollar sign. For example:
curl -dH -X POST https://\$my-todo-app:abc123@...etc
• This should return a link to a log-stream relating to the re-pulling of the image and restarting the app.
```
Add the webhook URL to our CI/CD pipeline code:
curl -dH -X POST https://$$rkmishratodo:$WEBHOOK_PASSWORD_KEY@rkmishratodo.scm.azurewebsites.net/api/registry/webhook
