
from http.client import responses
from urllib import response
import os, json
from flask import Flask, render_template, request, redirect
from Module1Exercise.data.trello_items import get_items, add_Card, update_InProgress, update_Done
from Module1Exercise.flask_config import Config

def create_app():
    app = Flask(__name__)

    headers = {
         "Accept": "application/json"
    }
    #Import all environment variables to be used in the application during runtime

    @app.route("/")
    def index():
        items = get_items()
        item_view_model = ViewModel(items)
        return render_template("index.html", view_model=item_view_model)

    @app.route("/addCard", methods=["POST"])
    def addToDoCard():
        add_Card(title=request.form.get("card_name"))
        return redirect('/')
            
    @app.route("/InProgress", methods=["POST"])
    def updateToInProgress():
        update_InProgress(updateid=request.form.get("in_progress"))
        return redirect("/")

    @app.route("/done", methods=["POST"])
    def updateToDone():
        update_Done(completeid=request.form.get("complete_status"))
        return redirect("/")
    return app

app = create_app()

class ViewModel:
    def __init__(self, items):
        self._items = items
        self._doingitems = []
        self._doneitems = []
        self._todoitems = []
        self._unknownstatusitems = []

        for item in items:
            match item.status:
                case "Doing":
                    self._doingitems.append(item)
                case "Done":
                    self._doneitems.append(item)
                case "To Do":
                    self._todoitems.append(item)
                case _:
                    self._unknownstatusitems.append(item)

    @property
    def allitems(self):
        return self._items

    @property
    def doing_items(self):
        return self._doingitems

    @property
    def done_items(self):
        return self._doneitems

    @property
    def todo_items(self):
        return self._todoitems

    @property
    def unknownstatusitems_items(self):
        return self._unknownstatusitems
