import requests
import os

class Item:
    def __init__(self, id, name, status="To Do"):
        self.id = id
        self.name = name
        self.status = status
        self.done = status == "Done"

    @classmethod
    def from_trello_card(cls, card, list):
        return cls(card["id"], card["name"], list["name"])


def get_items():
    """
    Fetches all Open cards from Trello.

    Returns:
        list: The list of items and their state.
    """
    payload = {
        "key": os.getenv("API_KEY"),
        "token": os.getenv("SERVER_TOKEN"),
        "cards": "open",
    }
    todoboardid = os.getenv("BOARD_ID")
    response = requests.get(
        f"https://api.trello.com/1/boards/{todoboardid}/lists/",
        payload,
    )
    json_res = response.json()
    listitems = []
    for list in json_res:
        cards = list["cards"]
        for card in cards:
            listitems.append(Item.from_trello_card(card, list))
    return listitems


def add_Card(title):
    
    """
    Adds a new Card with the card title entered to the ToDo List.

    Args:
        title: The title of the item.

    """

    api_key = os.getenv('API_KEY')
    server_token = os.getenv('SERVER_TOKEN')
    todo_list_id = os.getenv("TODO_LIST_ID")

    payload = {"name": title, "idList": todo_list_id}

    response = requests.post(
        f"https://api.trello.com/1/cards/?key={api_key}&token={server_token}", payload
    )


def update_InProgress(updateid):
    
    """
    Update the card status from ToDo to In-Progress.

    Args:
        title: The card title of the item which needs to be updated.

    """

    api_key = os.getenv('API_KEY')
    server_token = os.getenv('SERVER_TOKEN')
    doing_list_id = os.getenv("DOING_LIST_ID")

    payload = {"idList": doing_list_id}

    response = requests.put(
        f"https://api.trello.com/1/cards/{updateid}/?key={api_key}&token={server_token}",
        payload,
    )


def update_Done(completeid):
    
    """
    Update the card status from In-Progress to Done

    Args:
        title: The card title of the item which needs to be updated

    """

    api_key = os.getenv('API_KEY')
    server_token = os.getenv('SERVER_TOKEN')
    done_list_id = os.getenv("DONE_LIST_ID")

    data = {"idList": done_list_id}

    response = requests.put(
        f"https://api.trello.com/1/cards/{completeid}/?key={api_key}&token={server_token}",
        data,
    )
