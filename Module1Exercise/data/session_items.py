from flask import session
from Module1Exercise.flask_config import Config
_DEFAULT_ITEMS = [
    { 'id': 1, 'status': 'Not Started', 'Subtitle': 'List saved todo items' },
    { 'id': 2, 'status': 'Not Started', 'Subtitle': 'Allow new items to be added' }
]

# original file with minor changes in variable names

def get_items():
    """
    Fetches all saved items from the session.
    Returns:
        list: The list of saved items.
    """
    return session.get('todo_items', _DEFAULT_ITEMS.copy())

def get_item(id):
    """
    Fetches the saved item with the specified ID.
    Args:
        id: The ID of the item.
    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    items = get_items()
    return next((item for item in items if item['id'] == int(id)), None)


def add_item(title_name):
    """
    Adds a new item with the specified title to the session.
    Args:
        title: The title of the item.
    Returns:
        item: The saved item.
    """
    todo_items = get_items()

    # Determine the ID for the item based on that of the previously added item
    id = todo_items[-1]['id'] + 1 if todo_items else 0

    item = { 'id': id, 'Subtitle': title_name, 'status': 'Not Started' }

    # Add the item to the list
    todo_items.append(item)
    session['todo_items'] = todo_items

    return item

def save_item(item):
    """
    Updates an existing item in the session. If no existing item matches the ID of the specified item, nothing is saved.
    Args:
        item: The item to save.
    """
    existing_items = get_items()
    updated_items = [item if item['id'] == existing_item['id'] else existing_item for existing_item in existing_items]

    session['items'] = updated_items

    return item
