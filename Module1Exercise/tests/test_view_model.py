import pytest

from Module1Exercise.todoapp import ViewModel
from Module1Exercise.data.trello_items import Item


@pytest.fixture
def viewmodel() -> ViewModel:
    todolistitems = []
    todolistitems.append(Item(1, "Test: Todo1", "ToDo"))
    todolistitems.append(Item(2, "Test: Todo2", "ToDo") )
    todolistitems.append(Item(3, "Test: Todo3", "ToDo") )
    todolistitems.append(Item(4, "Test: Doing1", "Doing") )
    todolistitems.append(Item(5, "Test: Done1", "Done") )
    todolistitems.append(Item(6, "Test: Doing3", "Doing") )
    todolistitems.append(Item(7, "Test: Todo9", "ToDo") )
    return ViewModel(items=todolistitems)

def test_doing_items_count(viewmodel: ViewModel):
    assert len(viewmodel.doing_items)==2

