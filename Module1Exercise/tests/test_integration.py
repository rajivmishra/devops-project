import os
import json
import pytest
import requests
from dotenv import load_dotenv, find_dotenv
from Module1Exercise import todoapp

def test_index_page(monkeypatch, client):
    # Replace call to requests.get(url) with our own function
    monkeypatch.setattr(requests, 'get', get_lists_stub)
    response = client.get('/')

class StubResponse():
    def __init__(self, fake_response_data):
        self.fake_response_data = fake_response_data
    def json(self):
        return self.fake_response_data

def get_lists_stub(url, params):
    test_board_id = os.environ.get('TRELLO_BOARD_ID')
    fake_response_data = None
    print("hiii",url,test_board_id)
    if url == f'https://api.trello.com/1/boards/{test_board_id}/lists/':
        fake_response_data = [{
        'id': '123abc',
        'name': 'To Do',
        'cards': [{'id': '456', 'name': 'Test card'}]
        }]
    print(fake_response_data)
    return StubResponse(fake_response_data)


@pytest.fixture
def client():
    file_path= find_dotenv(".env.test")
    load_dotenv(file_path, override=True)
    test_app = todoapp.create_app()

    with test_app.test_client() as client:
        yield client

def test_index_page(monkeypatch, client):
    monkeypatch.setattr(requests, 'get', get_lists_stub)
    response_data = client.get("/")
    assert response_data