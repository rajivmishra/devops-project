#/bin/sh
sudo yum update -y
sudo yum groupinstall "Development Tools" -y
sudo yum install libffi-devel bzip2-devel wget openssl11-devel -y

wget https://www.python.org/ftp/python/3.10.2/Python-3.10.2.tgz
tar -xf Python-3.10.2.tgz
cd Python-3.10.2/

sudo ./configure --enable-optimizations
sudo make -j $(nproc)
sudo make altinstall